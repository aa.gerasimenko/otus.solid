﻿namespace Solid
{
    public class Report
    {
        // move to main
        // var builder = new ReportBuilder();
        // var sender = new EmailReportSender();
        // var reporter = new Reporter(builder, sender);

        // reporter.SendReports();
    }

    public interface IReportBuilder
    {
        IList<Report> CreateReports();
    }

    public interface IReportSender
    {
        void Send(Report report);
    }

    public class ReportBuilder : IReportBuilder
    {
        public IList<Report> CreateReports()
        {
            //
            return new List<Report>();
        }
    }

    public class EmailReportSender : IReportSender
    {
        public void Send(Report report)
        {
            ///
        }
    }

    public class Reporter // : IReporter
    {
        private readonly IReportBuilder reportBuilder;
        private readonly IReportSender reportSender;

        public Reporter(IReportBuilder reportBuilder, IReportSender reportSender)
        {
            this.reportBuilder = reportBuilder;
            this.reportSender = reportSender;
        }

        public void SendReports()
        {
            IList<Report> reports = reportBuilder.CreateReports();

            if (reports.Count == 0)
                throw new Exception("NoReports");

            foreach (Report report in reports)
            {
                reportSender.Send(report);
            }
        }
    }
}