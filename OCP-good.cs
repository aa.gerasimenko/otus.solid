﻿namespace Solid
{
    internal interface IAdapter
    {
        string Request(string url);
    }

    internal class AjaxAdapter : IAdapter
    {
        public string Request(string url) => "get ajax request";
    }

    internal class NodeAdapter : IAdapter
    {
        public string Request(string url) => "";
    }

    internal class HttpRequester
    {
        private IAdapter _adapter;

        public HttpRequester(IAdapter adapter)
        {
            _adapter = adapter;
        }

        public string Fetch(string url)
        {
            return _adapter.Request(url);
        }
    }
}