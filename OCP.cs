﻿namespace SolidNotGood
{
    internal abstract class AdapterBase
    {
        protected string Name;
        public string GetName() => Name;
    }
    internal class AjaxAdapter : AdapterBase
    {
        public AjaxAdapter()
        {
            Name = "ajax";
        }
    }
    internal class NodeAdapter : AdapterBase
    {
        public NodeAdapter()
        {
            Name = "node";
        }
    }

    internal class HttpRequester
    {
        private AdapterBase _adapter;

        public HttpRequester(AdapterBase adapter)
        {
            _adapter = adapter;
        }

        public string Fetch(string url)
        {
            var name = _adapter.GetName();
            if (name == "ajax")
                return AjaxCall(url);
            if (name == "node")
                return HttpCall(url);

            throw new Exception("");
        }

        private string AjaxCall(string url) => "get ajax request";
        private string HttpCall(string url) => "";
    }
}