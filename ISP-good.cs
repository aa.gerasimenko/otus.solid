﻿namespace Solid
{
    internal interface IWorkable
    {
        void Work();
    }
    internal interface IEatable
    {
        void Eat();
    }


    internal class Human : IEatable, IWorkable
    {
        public void Eat()
        {
        }

        public void Work()
        {
        }
    }

    internal class Robot : IWorkable
    {
        public void Work()
        {
        }
    }
}