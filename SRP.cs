﻿namespace SolidNotGood
{
    internal class User
    {
        public bool IsAdmin => true;
    }
    internal class Settings
    {
    }

    internal class UserSettings
    {
        private User User;

        public UserSettings(User user)
        {
            User = user;
        }

        public void ChangeSettings(Settings settings)
        {
            if (VerifyCredentials())
            {
                // ...
            }
        }

        private bool VerifyCredentials()
        {
            return User.IsAdmin;
        }
    }
}