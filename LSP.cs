﻿namespace SolidNotGood
{
    internal interface IOtusList
    {
        void Add(int value);
    }

    internal class List : IOtusList
    {
        private readonly IList<int> innerList = new List<int>();

        public void Add(int value)
        {
            innerList.Add(value);
        }
    }

    internal class DuplicateList : IOtusList
    {
        private readonly IList<int> innerList = new List<int>();

        public void Add(int value)
        {
            innerList.Add(value);
            innerList.Add(value);
        }
    }
}